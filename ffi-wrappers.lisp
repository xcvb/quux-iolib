;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; indent-tabs-mode: nil -*-
;;;
;;; --- FFI wrappers.
;;;
(in-package :quux-iolib)

(c "#ifdef __linux__")
(define "_GNU_SOURCE")
(include "unistd.h")
(include "sys/signalfd.h")
(include "signal.h")
(include "fcntl.h")
(c "#else")
(include "unistd.h")
(include "signal.h")
(include "fcntl.h")
(c "struct signalfd_siginfo { unsigned int ssi_signo; };")
(c "#  ifndef O_CLOEXEC")
(define "O_CLOEXEC" 0)
(c "#  endif")
(c "#endif")

#|
(declaim (inline install-signalfd))
(defwrapper ("install_signalfd" install-signalfd) :int
  ((signum :int)
   (sa-flags :int)))
|#

(include "stdio.h")
(include "stdlib.h")
(include "string.h")
(include "assert.h")
(include "errno.h")

(c"
#if defined(__linux__) && !defined(FORCE_SIGNALFD_EMULATION)
#  define signalfd__(fd,mask,flags) signalfd(fd,mask,flags)
#  define pipe2__(pipefd,flags) pipe2(pipefd,flags)
#else /* not __linux__ */
#  define signalfd__(fd,mask,flags) (-1)
static inline int pipe2__ (int pipefd[2], int flags) {
        int ret = pipe(pipefd);
        if (ret != 0) { return -1; }
        if (flags&O_NONBLOCK) {
                fcntl(pipefd[0],F_SETFL,O_NONBLOCK);
        }
        return 0;
}
#endif
")

(c"
static struct signalfd_params {
        int read_fd;
        int write_fd;
} *(signalfd_params[NSIG]);
")

(c"
static void signalfd_emulator (int signum) {
        struct signalfd_siginfo ssi;
        struct signalfd_params *params = signalfd_params[signum];
        int ret;

        assert (params != NULL);
        assert (params->write_fd != -1);
        memset(&ssi,0,sizeof(ssi));
        ssi.ssi_signo = signum;
notify_listener:
        ret = write(params->write_fd,&ssi,sizeof(ssi));
        if (ret == sizeof(ssi)) {
                return;
        }
        if (ret == -1) {
                switch (errno) {
                case EAGAIN:
                        /* The pipe buffer is full, therefore there's already
                           a notification in the pipe pending reception.
                           We can drop the redundant signal notification.
                           Redundant signals are NEVER guaranteed not to be
                           dropped anyway, and signals may have been dropped
                           on our way already if the system was busy. */
                        return;
                case EINTR:
                        /* Some other signal trumped us; try again! */
                        goto notify_listener;
                }
        }
        /* Something went deeply wrong. Abort. */
        abort();
}

static void error_abort (const char* msg, int perrorp) {
        if (perrorp) {
                perror(msg);
        } else {
                fprintf(stderr,\"%s\\n\",msg);
        }
        abort();
}")

(c"
static void warning_signal_handler (int signum) {
	char msg[128];
	int i = snprintf(msg,sizeof(msg),
			\"\\nCaught signal %d, which shouldn't have happened.\\n\",signum);
	write(2,msg,i);
}")

(c"
extern int install_signalfd(int signum, int sa_flags) {
        int pipefd[2];
        int ret;
        sigset_t sigmask;
        int emulate_signalfd;
        struct signalfd_params *params;
        struct sigaction sa;

        if ( (signum<0) || (signum>=NSIG) ) {
                error_abort(\"install_signalfd called with bad signal number.\",0);
        }

        /* Setup sigaction */
        memset(&sa,0,sizeof(sa));
        sa.sa_flags = (sa_flags & (SA_NOCLDSTOP|SA_NOCLDWAIT))|SA_ONSTACK;

        /* Create mask with one signal */
        sigemptyset(&sigmask);
        sigaddset(&sigmask,signum);

        /* Allocate parameters */
        if (signalfd_params[signum]) {
                error_abort(\"install_signalfd already installed.\",0);
        }
        params = malloc(sizeof(signalfd_params));
        if (params == NULL) {
                error_abort(\"install_signalfd malloc failed.\",0);
        }

        /* Before we touch the handler, block the signal */
        ret = sigprocmask(SIG_BLOCK, &sigmask, NULL);
        if (ret != 0) { error_abort(\"install_signalfd signal blocking failed.\",0); }

        /* First, try signalfd */
        ret = signalfd__(-1,&sigmask,SFD_CLOEXEC|SFD_NONBLOCK);
        if (ret != -1) { /* success with signalfd, instead default handler! */
                emulate_signalfd = 0;
                sa.sa_handler = &warning_signal_handler; /* was SIG_DFL, but we want to catch bugs */
                params->read_fd = ret;
                params->write_fd = -1;
                goto signalfd_sigaction;
        }

        /* no success with signalfd (probably EINVAL), emulate! */
        emulate_signalfd = 1;
        sa.sa_handler = &signalfd_emulator;
        ret = pipe2__(pipefd,O_CLOEXEC|O_NONBLOCK);
        if (ret != 0) { error_abort(\"install_signalfd pipe2 failed: \",1); }
        params->read_fd = pipefd[0];
        params->write_fd = pipefd[1];

  signalfd_sigaction:
        signalfd_params[signum] = params;
        ret = sigaction(signum,&sa,NULL);
        if (ret != 0) { error_abort(\"install_signalfd failed sigaction: \",1); }
        if (emulate_signalfd) {
                ret = sigprocmask(SIG_UNBLOCK, &sigmask, NULL);
                if (ret != 0) { error_abort(\"install_signalfd signal unblocking failed.\",0); }
        }
        return params->read_fd;
}")

(c"
extern void uninstall_signalfd(int signum, int block) {
        int ret;
        sigset_t sigmask;
        struct signalfd_params *params;
        struct sigaction sa;

        if ( (signum<0) || (signum>=NSIG) ) {
                error_abort(\"install_signalfd called with bad signal number.\",0);
        }

        /* Setup sigaction */
        memset(&sa,0,sizeof(sa));
        sa.sa_handler = SIG_DFL;

        /* Create mask with one signal */
        sigemptyset(&sigmask);
        sigaddset(&sigmask,signum);

        /* Before we touch the handler, block the signal */
        ret = sigprocmask(SIG_BLOCK, &sigmask, NULL);
        if (ret != 0) { error_abort(\"uninstall_signalfd signal blocking failed.\",0); }

        /* Release parameters */
        params = signalfd_params[signum];
        if ( params == NULL) {
                error_abort(\"uninstall_signalfd not installed.\",0);
        }
        close(params->read_fd);
        if (params->write_fd != -1) {
                close(params->write_fd);
        }
        free(params);
        signalfd_params[signum] = NULL;

        ret = sigaction(signum,&sa,NULL);
        if (ret != 0) { error_abort(\"uninstall_signalfd failed sigaction: \",1); }
        if (!block) {
                ret = sigprocmask(SIG_UNBLOCK, &sigmask, NULL);
                if (ret != 0) { error_abort(\"uninstall_signalfd signal unblocking failed.\",0); }
        }
}")
