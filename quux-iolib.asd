;;; -*- mode: lisp -*-

(in-package :asdf)

(defsystem :quux-iolib
  :defsystem-depends-on (:cffi-grovel)
  :depends-on (:iolib/os)
  :components
  ((:file "pkgdcl")
#|   (:cffi-wrapper-file "ffi-wrappers" ;; NO: rely on libfixposix instead.
    :soname "libquux-iolib" :depends-on ("pkgdcl")) |#
   (:cffi-grovel-file "ffi-grovel" :depends-on ("pkgdcl"))
   (:file "misc" :depends-on ("pkgdcl"))
   (:file "signalfd" :depends-on ("ffi-grovel" #|"ffi-wrappers"|#))))
