#+xcvb (module (:depends-on ("pkgdcl")))

(in-package :quux-iolib)

;;; file-fd-stream

(defclass file-fd-stream (dual-channel-gray-stream)
  ((path :initarg :path :reader file-stream-path)))

(defmethod print-object ((o file-fd-stream) s)
  (with-slots (path (fd iolib.streams::fd) (ef iolib.streams::external-format)
                    (ib iolib.streams::input-buffer) (ob iolib.streams::output-buffer)) o
    (print-unreadable-object (o s :type nil :identity t)
      (format s "~A ~S ~S ~S ~S ~S ~S/~S ~S ~S/~S ~S (~S ~S ~S)"
              'file-fd-stream :fd fd :path path
              :ibuf (iolib.streams::iobuf-length ib) (iolib.streams::iobuf-size ib)
              :obuf (iolib.streams::iobuf-length ob) (iolib.streams::iobuf-size ob)
              :ef (babel-encodings:enc-name (babel:external-format-encoding ef))
              :eol-style (babel:external-format-eol-style ef)))))

(defun make-file-stream (path &key
                         (direction :input)
                         (if-exists :error)
                         (if-does-not-exist (ecase direction
                                              (:input :error)
                                              ((:io :output) :create)))
                         (external-format :default)
                         (block t))
  ;; Very ad-hoc: doesn't do :DIRECTION :PROBE correctly, or handle most errors,
  ;; :IF-DOES-NOT-EXIST, among many other things.
  (check-type direction (member :input :output :io :probe))
  (check-type if-exists (member :error :new-version :rename :rename-and-delete
                                :overwrite :append :supersede nil))
  (check-type if-does-not-exist (member nil :error :create))
  (when (member if-exists '(:new-version :rename))
    (error "if-exists=~S not implemented"
           if-exists))
  (loop
   (handler-case
       (isys:open
        (namestring path)
        (logior (if block 0 isys:o-nonblock)
                (if (and (member direction '(:output :io))
                         (member if-does-not-exist '(:create :rename-and-delete)))
                    isys:o-creat 0)
                (ecase direction
                  (:input isys:o-rdonly)
                  (:output isys:o-wronly)
                  (:io isys:o-rdwr)
                  (:probe #+linux 3 #-linux (error "Cannot open in probe mode")))
                (ecase if-exists
                  ((nil :error :new-version :rename :rename-and-delete) isys:o-excl)
                  (:supersede isys:o-trunc)
                  (:append isys:o-append)
                  (:overwrite 0)))
        (logior isys:s-irusr isys:s-iwusr))
     (:no-error (fd)
       (return (make-instance 'file-fd-stream
                              :path path
                              :fd fd
                              :external-format external-format)))
     (isys:eintr (c)
       (if block (error c) nil))
     (isys:eexist (c)
       (ecase if-exists
         ((nil) (return nil))
         (:error (error c))
         (:rename-and-delete
          (delete-file path)
          (setf if-exists :supersede if-does-not-exist :create))
         ((:rename :new-version)
          (error "not implemented")))))))

(defmacro with-open-file-stream ((var path &rest options) &body body)
  (with-gensyms (stream)
    `(let ((,stream (make-file-stream ,path ,@options)))
       (with-open-stream (,var ,stream)
         ,@body))))

(defun wexitsuccess (status)
  (and (isys:wifexited status)
       (zerop (isys:wexitstatus status))))

(defun run-program/ (command
                     &rest keys
                     &key output ignore-error-status force-shell
                     (element-type xcvb-driver:*default-element-type*)
                     (external-format :default)
                     &allow-other-keys)
  (let* ((process
          (iolib.os:create-process
           command
           :search t :environment t
           :stdin (if (eq output :interactive) t :null)
           :stdout (case output
                     ((:interactive nil) 1)
                     (otherwise :pipe))
           :stderr t
           :external-format external-format))
         (stream (iolib.os:process-stdout process)))
    (unwind-protect
         (xcvb-driver:slurp-input-stream output stream)
      (close process)
      (let ((status (iolib.os:process-status process)))
        (unless (or ignore-error-status (wexitsuccess status))
          (cerror "ignore error code~*~*"
                  "Process ~S exited with error code ~D (status #x~X)"
                  command (isys:wexitstatus status) status))))))
