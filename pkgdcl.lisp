#+xcvb (module (:description "package for QUUX IOLib"))

(in-package :cl)

(defpackage :quux-iolib
  (:use :iolib.os :iolib.base :iolib.streams :alexandria :cffi)
  (:export #:install-signalfd #:uninstall-signalfd
           #:signalfd-signalled-p
           #:make-file-stream #:with-open-file-stream
           #:run-program/process-output-stream
           #:wexitsuccess))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (pushnew :quux-iolib *features*))
