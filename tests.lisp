(in-package :quux-iolib)

(declaim (optimize (debug 3) (safety 3) (speed 2)
                   #+sbcl (sb-ext:inhibit-warnings 3)))

(defvar *event-base* nil)
(defvar *sigchldfd* nil)

(defvar *children* 0)

(defun spawn-child ()
  (incf *children*)
  (let ((pid (isys::fork)))
    (when (zerop pid)
      (sleep-and-die))
    (format t "Spawned child ~D~%" pid)
    (finish-output)
    pid))

(defun sleep-and-die ()
  (sleep 2)
  (isys::exit 42))

(defun make-sigchldfd-handler ()
  (lambda (fd event exception)
    (declare (ignore event exception))
    (when (signalfd-signalled-p fd)
      (handle-signalling-children
       (lambda (pid status)
         (when (isys::wifexited status)
           (format t "Child ~D returned with status ~D~%"
                   pid (isys::wexitstatus status))
           (finish-output)
           (decf *children*)))))))

(defun make-echoer (stream id disconnector)
  (lambda (fd event exception)
    (declare (ignore fd event exception))
    (handler-case
        (let ((line (read-line stream)))
          (cond ((string= line "quit")
                 (funcall disconnector))
                (t
                 (format t "~A: ~A~%" id line)
                 (finish-output)
                 (ignore-some-conditions (iolib.streams:hangup)
                   (finish-output stream)))))
      (end-of-file ()
        (funcall disconnector)))))

(defun test-qi (&optional (children 7))
  (setf *sigchldfd* (install-signalfd isys::sigchld isys::sa-nocldstop))
  (setf *event-base* (make-instance 'iomux:event-base
                                    #|:mux 'iomux:select-multiplexer|#))
  (iomux:set-io-handler
   *event-base* *sigchldfd*
   :read (make-sigchldfd-handler))
  (iomux:set-io-handler
   *event-base* 0
   :read (make-echoer *standard-input* "stdin"
                      (lambda (&rest r)
                        (declare (ignore r))
                        (format t "Got EOF on stdin~%")
                        (finish-output)
                        (isys::exit 42))))
  (loop :repeat children :do (spawn-child))
  (loop :until (zerop *children*) :do
    (iomux:event-dispatch *event-base* :one-shot t))
  (close *event-base*)
  (uninstall-signalfd isys::sigchld 1)
  (values))
