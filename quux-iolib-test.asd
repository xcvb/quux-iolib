;;; -*- mode: lisp -*-

(in-package :asdf)

(defsystem :quux-iolib-test
  :depends-on (:quux-iolib)
  :components
  ((:file "tests")))
