;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; indent-tabs-mode: nil -*-
;;;
(in-package :quux-iolib)

(c "#ifdef __linux__")
(define "_GNU_SOURCE")
(c "#endif")
(include "unistd.h")
(include "signal.h")
#+linux
(include "sys/signalfd.h")
(include "fcntl.h")
#-linux
(c "struct signalfd_siginfo { unsigned int ssi_signo; };")

(ctype ssi-signo-t #+linux "uint32_t" #-linux "unsigned int")

(cstruct signalfd-siginfo "struct signalfd_siginfo"
  (signo  "ssi_signo" :type ssi-signo-t))

(define "sizeof_signalfd_siginfo" "sizeof(struct signalfd_siginfo)")

(constant (size-of-signalfd-siginfo "sizeof_signalfd_siginfo"))
