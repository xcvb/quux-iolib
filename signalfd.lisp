#+xcvb (module (:depends-on ("ffi-wrappers" "ffi-grovel")))

(in-package :quux-iolib)

(defcfun (lfp-install-signalfd "lfp_install_signalfd") :int
  "install_signalfd installs a signalfd handler for the specified SIGNUM,
returning a fd in non-blocking mode that, when read,
contains a struct signalfd_siginfo for every intercepted signal.
When handling SIGCLD, sa_flags may specify SA_NOCLDSTOP or SA_NOCLDWAIT
as specified in sigaction(2).
"
  ;; Whoever handles the signal must make sure to
  ;; (a) keep reading from the fd until it has no more data, and
  ;; (b) every time data is found,
  ;;   make sure to keep handling the sources of signals
  ;;   until otherwise informed that there are no more such sources
  ;;   (e.g. when handling SIGCLD, waitpid returns 0).
  ;; 
  ;; We use the signalfd system call when available, then blocking the signal.
  ;; When it is not available we instead install a signal handler
  ;; that emulates the behavior of signalfd, and unblock the signal.
  ;; In that case, a structure of the correct size is communicated,
  ;; but only the field ssi_signo will contain a valid value.
  (signum  :int)
  (sa-flags :int)
  (blockp :pointer)) ;; bool*

(defun install-signalfd (signum sa-flags)
  #| TODO: Fix me with input from nyef?
  #+sbcl
  (macrolet ((delsig (sigset signum)
               `(sb-alien:alien-funcall
                 (sb-alien:extern-alien "sigdelset" (function sb-alien:int (* t) sb-alien:int))
                 (sb-alien:addr (sb-alien:extern-alien ,sigset sb-alien:int)) ,signum)))
    (delsig "blockable_sigset" signum)
    (delsig "deferrable_sigset" signum))|#
  (with-foreign-object (blockp :int)
    (let ((fd (lfp-install-signalfd signum sa-flags blockp)))
      (values fd (plusp (mem-ref blockp :int))))))

(defcfun (lfp-uninstall-signalfd "lfp_uninstall_signalfd") :void
  (signum :int)
  (block :int)) ;; bool

(defun signalfd-signalled-p (fd &optional signum)
  "Given real or emulated signalfd FD, consume all available data from the FD.
If SIGNUM is specified, check that data is indeed associated to it.
Return T if there was any data, NIL otherwise."
  (with-foreign-object (ssi 'signalfd-siginfo)
    (loop :with signalled = 0
      :for length =
      (loop
        (handler-case
            (isys:read fd ssi size-of-signalfd-siginfo)
          (isys:ewouldblock () (return 0))
          (isys:eintr () nil)
          (:no-error (x)
            (return x))))
      :do
      (when (zerop length)
        (return (and (not (zerop signalled)) signalled)))
      (incf signalled)
      (assert (= length size-of-signalfd-siginfo))
      (when signum
        (with-foreign-slots ((signo) ssi signalfd-siginfo)
          (assert (eql signo signum)))))))
